Phabaste
========

A command-line tool for Phabricator's Paste application that you'll love more
than the name.

Phabaste by default operates in a simple I/O mode:

```
    $ make 2>&1 | phabaste create --title 'oh dear' --project kernel
    $ phabaste get -o curl-pipe-bin-sh.sh P999
```

It can also do more clever things, which you can find out about via `--help`.


Installation
===========

Just install it in your path and go!

```$ ln -s $(pwd)/phabaste ~/.local/bin```

[python-phabricator](https://github.com/disqus/python-phabricator) must be
installed, either through your distribution or via `pip install phabricator`.

If you don't have any Phabricator hosts configured locally, you may be prompted
to log in.


Contributing
============

Phabaste uses Phabricator (what else?) for contributions, and `git-phab`.
Please see the [getting-started documentation]( https://phabricator.freedesktop.org/w/git-phab/).
No copyright assignment is required, but you must be legally able to contribute
the code under the same licence.


Distribution
============

Phabaste is licensed under the MIT/X11 licence.
